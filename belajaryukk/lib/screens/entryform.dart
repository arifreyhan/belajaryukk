import 'package:code_editor/code_editor.dart';
import 'package:flutter/material.dart';
class Entryform extends StatefulWidget {
  Entryform({Key key}) : super(key: key);

  @override
  _EntryformState createState() => _EntryformState();
}

class _EntryformState extends State<Entryform> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(child: Container(
        margin: EdgeInsets.all(20),
        child: Column(children: [
          Container(alignment: Alignment.topLeft,
            child: Text("Saran",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),)
          ),
         Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                             right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            decoration: InputDecoration(
                                labelText: "Isi text...",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => !input.contains("@")
                                ? "Jangan Kosong"
                                : null,
                            onSaved: (input) => print(input),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 13,
                      ),
                       Container(alignment: Alignment.topLeft,
                         child: Text("Masukkan",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),)
                       ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(
                              right: 15.0, top: 3, bottom: 3),
                          child: TextFormField(
                            decoration: InputDecoration(
                                labelText: "Isi text...",
                                labelStyle: TextStyle(color: Colors.grey)),
                            validator: (input) => input.isEmpty
                                ? "Jangan Kosong"
                                : null,
                            onSaved: (input) => print(input),
                            obscureText: true,
                          ),
                        ),
                      ),
      
                      SizedBox(
                        height: 13,
                      ),
                      Container(
                        child: InkWell(
                          onTap: (){},
                          child: Container(
                            margin: EdgeInsets.all(0),
                            width: double.infinity,
                            height: 70,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.blueAccent),
                            child: Center(child: Text("Kirim", style: TextStyle(color: Colors.white),)),
                          ),
                        ),
                      ),
      ],),)),
    );
  }
}