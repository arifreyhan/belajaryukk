import 'dart:ui';

import 'package:flutter/material.dart';

class Aboutpage extends StatefulWidget {
  Aboutpage({Key key}) : super(key: key);

  @override
  _AboutpageState createState() => _AboutpageState();
}

class _AboutpageState extends State<Aboutpage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Belajar Yukk"),),
      body: SafeArea(child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top:30, left:20, right:20),
       child: Column(
         children: [
           Container(
             child: Image.asset("images/css3.png")
           ),
           SizedBox(
             height: 20,
           ),
           Container(
             child: Text("Belajar Yukk merupakan aplikasi media belajar khusus tentang materi Pemrograman Web secara online yang disediakan untuk seluruh masyarakat Indonesia.",style: TextStyle(fontSize: 18, ),textAlign: TextAlign.center,)
           ),
           SizedBox(
             height: 150,
           ),
           Container(
             alignment: Alignment.bottomCenter,
              color: Colors.grey[300],
            //  color: Colors.black45,
             padding: EdgeInsets.all(21),
             child: Column(
               children: [
                 _copyright("Copyright by"),
                 _copyright("Arif Reyhan Febrian"),
                 _copyright("18282003")

               ],
             ),
           )
         ],),
    ),)
    );
  }

  Widget _copyright(String data){
    return Text(data, style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),);
  }
}